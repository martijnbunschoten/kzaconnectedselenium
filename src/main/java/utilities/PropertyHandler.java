package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyHandler {


    private static PropertyHandler Instance = null;
    private final Properties properties;

    private PropertyHandler () throws IOException {
        this.properties = new Properties();
        InputStream input = new FileInputStream("src/main/resources/global.properties");
        properties.load(input);
    }

    public static PropertyHandler getInstance() {
        if(Instance == null) {
            try {
                Instance = new PropertyHandler();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return Instance;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }
}
