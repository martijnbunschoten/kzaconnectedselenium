package utilities;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Driver {

    private static WebDriver instance = null;

    public static void initialize() throws MalformedURLException {

        if(instance == null) {
            System.out.println("Initializing driver...");
            if(PropertyHandler.getInstance().getValue("browser").equals("chrome")) {
                System.setProperty("webdriver.chrome.driver", PropertyHandler.getInstance().getValue("chromedriverpath"));

                DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
                desiredCapabilities.setBrowserName("chrome");
                desiredCapabilities.setPlatform(Platform.LINUX);
                //instance = new RemoteWebDriver(new URL(PropertyHandler.getInstance().getValue("seleniumGrid")),desiredCapabilities);
                ChromeOptions options = new ChromeOptions();
                options.addArguments("incognito");
                instance = new ChromeDriver(options);

            } else if(PropertyHandler.getInstance().getValue("browser").equals("firefox")) {
                System.setProperty("webdriver.gecko.driver", PropertyHandler.getInstance().getValue("firefoxpath"));
                instance = new FirefoxDriver();
            } else if(PropertyHandler.getInstance().getValue("browser").equals("ie")) {
                System.setProperty("webdriver.ie.driver", PropertyHandler.getInstance().getValue("iedriverpath"));
                instance = new InternetExplorerDriver();
            } else {
                System.out.println("No driver found...");
            }
        }
        instance.manage().window().maximize();
        instance.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    public static WebDriver getWebdriver() {
            return instance;
    }

    public static void closeBrowser() {
        System.out.println("Closing the browser");
        instance.quit();
        instance = null;
    }
}
