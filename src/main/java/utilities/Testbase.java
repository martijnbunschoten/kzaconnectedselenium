package utilities;

import org.testng.annotations.AfterTest;
import pages.BasisPagina;
import pages.WelkomKzaPagina;

import java.net.MalformedURLException;

public class Testbase extends Driver {

    public static void setup() throws MalformedURLException {
         initialize();
    }

    @AfterTest
    public static void teardown() {
        closeBrowser();
    }


    public WelkomKzaPagina navigeerNaar(String pagina) {
        getWebdriver().navigate().to(BasisPagina.gaNaar(pagina));
        return new WelkomKzaPagina();
    }
}
