package models;

import utilities.PropertyHandler;

public class Navigatie {

    public static String getKzaStaging() {
         return PropertyHandler.getInstance().getValue("kzastaging");
    }

    public static String getKzaProduction() {
        return PropertyHandler.getInstance().getValue("kzaproduction");
    }
}
