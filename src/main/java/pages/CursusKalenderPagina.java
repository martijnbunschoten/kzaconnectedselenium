package pages;

import org.openqa.selenium.By;

public class CursusKalenderPagina extends BasisPagina {

    private static By cursusAanmakenKnop = By.xpath("//i[@class='plus icon']");
    private static By uitloggenKnop = By.xpath("//i[@class='sign out icon']");

    public CursusAanmakenPagina klikOpCursusAanmaken() {
        klikOpElement(cursusAanmakenKnop);
        return new CursusAanmakenPagina();
    }
}
