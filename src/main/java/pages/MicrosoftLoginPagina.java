package pages;

import org.openqa.selenium.By;

public class MicrosoftLoginPagina extends BasisPagina{

    private static By signInEmail = By.id("i0116");
    private static By nextKnop = By.id("idSIButton9");
    private static By enterPassword = By.id("i0118");
    private static By signInKnop = By.id("idSIButton9");
    private static By staySignedInYes = By.xpath("//input[@value='Yes']");
    private static By staySignedInNo = By.id("idBtn_Back");

   public void acceptPermission() {
       klikOpElement(nextKnop);
   }


   public CursusKalenderPagina loginOpMicrosoft(String mail, String wachtwoord) {
         vulTekstIn(signInEmail, mail);
         browserStop(2);
         klikOpElement(nextKnop);
         browserStop(2);
         vulTekstIn(enterPassword, wachtwoord );
         klikOpElement(signInKnop);
         browserStop(2);
         klikOpElement(staySignedInNo);

         return new CursusKalenderPagina();
   }

   public MicrosoftLoginPagina vulMicrosoftMailIn(String mail) {
       vulTekstIn(signInEmail, mail);
       klikOpVolgende();
       return new MicrosoftLoginPagina();
   }

   public void klikOpVolgende() {
       browserStop(2);

     klikOpElement(nextKnop);
     browserStop(2);


   }

}
