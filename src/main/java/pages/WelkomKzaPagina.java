package pages;

import org.openqa.selenium.By;

public class WelkomKzaPagina extends BasisPagina{

    private static By inloggenKnop = By.xpath("//i[@class='sign in icon']");


    public MicrosoftLoginPagina klikOpInloggen() {
        klikOpElement(inloggenKnop);
        return new MicrosoftLoginPagina();
    }
}
