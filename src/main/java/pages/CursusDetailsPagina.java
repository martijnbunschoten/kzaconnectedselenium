package pages;

import org.openqa.selenium.By;

public class CursusDetailsPagina extends BasisPagina {

     private static By cursusNaam = By.xpath("//label[contains(text(), 'Naam')]//following-sibling::div");
     private static By inschrijvingStatus = By.xpath("//label[contains(text(), 'Status')]//following-sibling::div");
     private static By aanmeldenKnop = By.xpath("//div[@class='ui right floated pink button']");

     public static String haalCursusNaamOp() {
            return haalTekstVanElementOp(cursusNaam);
     }

     public static String haalInschrijvingStatusOp() {
         return haalTekstVanElementOp(inschrijvingStatus);
     }

     public static void klikOpAanmelden() {
         klikOpElement(aanmeldenKnop);
     }
}
