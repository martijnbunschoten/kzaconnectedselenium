package pages;

import org.openqa.selenium.By;

public class CursusAanmakenPagina extends BasisPagina {

    private static By cursusNaam = By.name("cursus[naam]");

    private static By attitudeDropdown = By.name("cursus[attitude]");
    private static By testAttitude = By.xpath("//option[contains(text(), 'Test attitude')]");
    private static By technicalAttitude = By.xpath("//option[contains(text(), 'Technical attitude')]");
    private static By businessAttitude = By.xpath("//option[contains(text(), 'Technical attitude')]");

    private static By functieNiveauDropdown = By.name("cursus[functieniveau]");
    private static By functieNiveauTrainee = By.xpath("//option[contains(text(), 'Trainee')]");
    private static By functieNiveau1 = By.xpath("//option[contains(text(), '1')]");
    private static By functieNiveau2 = By.xpath("//option[contains(text(), '2')]");
    private static By functieNiveau3 = By.xpath("//option[contains(text(), '3')]");
    private static By functieNiveauNvt = By.xpath("//option[contains(text(), ' N.v.t. ')]");

    private static By slagingsCriteriumDropDown = By.name("cursus[slagingscriterium]");
    private static By criteriumAvondenVolgen = By.xpath("//option[contains(text(), 'Cursusavonden gevolgd')]");
    private static By criteriumCertificaat = By.xpath("//option[contains(text(), 'Certificaat behaald')]");
    private static By criteriumZelfstudie = By.xpath("//option[contains(text(), 'Zelfstudie')]");
    private static By criteriumPraktijkErvaring = By.xpath("//option[contains(text(), 'Praktijkervaring opgedaan')]");

    private static By curusStatusDropDown = By.name("cursus[status]");
    private static By statusNogInTePlannen = By.xpath("//option[contains(text(), 'Nog in te plannen')]");
    private static By statusInschrijvingOpen = By.xpath("//option[contains(text(), 'Inschrijving geopend')]");
    private static By statusInschrijvingDicht = By.xpath("//option[contains(text(), 'Inschrijving gesloten')]");

    private static By aantalDeelnemers = By.name("cursus[maxdeelnemers]");

    private static By naamDocent = By.xpath("//input[@placeholder='Naam']");
    private static By mailDocent = By.xpath("//input[@placeholder='Email']");

    private static By dataFormat = By.xpath("//input[@placeholder='Format: dd-mm-yyyy hh:mm:ss']");

    private static By cursusBeschrijving = By.name("cursus[beschrijving]");

    private static By cursusOpslaan = By.xpath("//div[contains(text(), 'Opslaan')]");


    public CursusAanmakenPagina vulCursusNaamIn(String cursusnaam) {
        vulTekstIn(cursusNaam, cursusnaam);
        return this;
    }

    public CursusAanmakenPagina selecteerAttitude(String attitude) {

        klikOpElement(attitudeDropdown);

        switch (attitude) {

            case "testAttitude":
                klikOpElement(testAttitude);
                break;
            case "technicalAttitude":
                klikOpElement(technicalAttitude);
                break;
            case "businessAttitude":
                klikOpElement(businessAttitude);
                break;
            default:
                System.out.println("Dit is geen geldige attitude, selecteer test, technical of business");
        }
        return this;
    }


    public CursusAanmakenPagina selecteerFunctieNiveau(String functieniveau) {

        klikOpElement(functieNiveauDropdown);

        switch (functieniveau) {

            case "trainee":
                klikOpElement(functieNiveauTrainee);
                break;
            case "1":
                klikOpElement(functieNiveau1);
                break;
            case "2":
                klikOpElement(functieNiveau2);
                break;
            case "3":
                klikOpElement(functieNiveau3);
                break;
            default:
                klikOpElement(functieNiveauNvt);
        }
        return this;
    }


    public CursusAanmakenPagina selecteerSlagingsciterium(String criterium) {

        klikOpElement(slagingsCriteriumDropDown);

        switch (criterium) {

            case "cursusAvondenVolgen":
                klikOpElement(criteriumAvondenVolgen);
                break;
            case "certificaatHalen":
                klikOpElement(criteriumCertificaat);
                break;
            case "zelfstudie":
                klikOpElement(criteriumZelfstudie);
                break;
            case "praktijkervaring":
                klikOpElement(criteriumPraktijkErvaring);
                break;
            default:
                System.out.println("Er is geen geldig cursus criterium opgegeven.");
        }
        return this;
    }

    public CursusAanmakenPagina selecteerCursusStatus(String status) {

        klikOpElement(curusStatusDropDown);

        switch(status) {

            case "inplannen":
                klikOpElement(statusNogInTePlannen);
                break;
            case "open":
                klikOpElement(statusInschrijvingOpen);
                break;
            case "dicht":
                klikOpElement(statusInschrijvingDicht);
                break;
            default: System.out.println("Er is geen geldige cursus status opgegeven");
        }
        return this;
    }

    public CursusAanmakenPagina vulAantalDeelnemersIn(String deelnemers) {
        vulTekstIn(aantalDeelnemers, deelnemers);
        return this;
    }

    public CursusAanmakenPagina vulNaamEnMailDocentIn(String naam, String mail) {
        vulTekstIn(naamDocent, naam);
        vulTekstIn(mailDocent, mail);

        return this;
    }


    public CursusAanmakenPagina vulDataCursusIn(String datum, String tijdstip) {
           vulTekstIn(dataFormat, datum+" "+tijdstip);
           return this;
    }

    public CursusAanmakenPagina vulCursusBeschrijvingIn(String beschrijving) {
        vulTekstIn(cursusBeschrijving, beschrijving);
        browserStop(2);
        return this;
    }

    public void klikOpOpslaan() {
        scrollNaarElement(cursusOpslaan);
        klikOpElement(cursusOpslaan);
    }
}


