package pages;

import models.Navigatie;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Driver;

public class BasisPagina extends Driver {

    protected static void klikOpElement(By element) {
        getWebdriver().findElement(element).click();
    }

    protected static String haalTekstVanElementOp(By element) {
        return getWebdriver().findElement(element).getText();
    }

    protected static void vulTekstIn(By element, String tekst) {
        getWebdriver().findElement(element).sendKeys(tekst);
    }

    public static String gaNaar(String pagina) {

        switch(pagina) {
            case "KzaStaging":
                return Navigatie.getKzaStaging();
            case "KzaProduction":
                return Navigatie.getKzaProduction();
            default:
                return (pagina + " is niet bekend. Geef een geldige URL op");
        }
    }


    protected static void drukOpEnter(By element) {
        getWebdriver().findElement(element).sendKeys(Keys.ENTER);
    }


    protected static void browserStop(int seconds) {

        try {
            Thread.sleep(seconds * 1000);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected static void WachtTotElementGeladenIs(By element) {
        new WebDriverWait(getWebdriver(), 10).until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    protected static void BrowserStop(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected static void scrollNaarElement(By element) {

        WebElement myElement = getWebdriver().findElement(element);
        ((JavascriptExecutor) getWebdriver()).executeScript("arguments[0].scrollIntoView();", myElement);
        Actions actions = new Actions(getWebdriver());
        actions.moveToElement(myElement).perform();
    }
}
