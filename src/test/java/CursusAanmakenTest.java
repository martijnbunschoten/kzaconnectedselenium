import models.Gebruiker;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.CursusDetailsPagina;
import utilities.Testbase;
import java.net.MalformedURLException;

public class CursusAanmakenTest extends Testbase {

    @BeforeTest
    public static void setupTest() throws MalformedURLException {
        setup();
    }

    @Test
    public void cursusAanmaken() {

         navigeerNaar("KzaStaging")
        .klikOpInloggen()
        .loginOpMicrosoft(Gebruiker.getMail(), Gebruiker.getWachtwoord())
        .klikOpCursusAanmaken()
        .vulCursusNaamIn("Testcursus")
        .selecteerAttitude("technicalAttitude")
        .selecteerFunctieNiveau("trainee")
        .selecteerSlagingsciterium("zelfstudie")
        .selecteerCursusStatus("open")
        .vulAantalDeelnemersIn("8")
        .vulNaamEnMailDocentIn("test", "mbunschoten@kza.nl")
        .vulDataCursusIn("05-06-2019", "18:00:00")
        .vulCursusBeschrijvingIn("automatische test TEST")
        .klikOpOpslaan();

        if("Testcursus".equals(CursusDetailsPagina.haalCursusNaamOp()) && "Inschrijving geopend".equals(CursusDetailsPagina.haalInschrijvingStatusOp())) {
            CursusDetailsPagina.klikOpAanmelden();
        }
    }
}
